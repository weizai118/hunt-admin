package com.hunt.service;

import java.io.IOException;
import java.util.Map;

/**
 * @Author ouyangan
 * @Date 2017/1/16/14:28
 * @Description 短信发送模块
 */
public interface SmsService {

    /**
     * @param phone
     * @param map
     * @return
     * @Author ouyangan
     * @Date 2017-1-16 14:33:11
     * @Description 发送单条短信
     */
    void send(String phone, Map<String, String> map) throws Exception;

    /**
     * @param phone
     * @return
     * @Author ouyangan
     * @Date 2017-1-16 14:33:11
     * @Description 查询短信发送记录
     */
    Object selectRecorder(String phone);

    /**
     * @param phones
     * @param map
     * @return
     * @Author ouyangan
     * @Date 2017-1-16 14:33:11
     * @Description 发送多条短信
     */
    Object batchSend(String phones, Map<String, String> map);
}
