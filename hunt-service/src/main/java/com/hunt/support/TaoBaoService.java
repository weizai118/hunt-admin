package com.hunt.support;

import java.io.IOException;
import java.util.Map;

/**
 * @Author: ouyangan
 * @Date : 2017/1/14
 * @Description
 */
public interface TaoBaoService {
    public void aLiDaYuSendMessage(String phone, Map<String,String> map) throws Exception;
}
